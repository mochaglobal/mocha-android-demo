package com.mocha.demokeyboard

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import com.mocha.demokeyboard.databinding.ViewMochaAboveKeyboardBinding

/**
 * A custom view that is displayed above the keyboard spelling suggestions.
 */
class MochaViewAboveKeyboard @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    private val binding =
        ViewMochaAboveKeyboardBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
    }

    fun setQuickLinksViewVisible(visible: Boolean) {
        binding.quickLinksView.visibility = if (visible) VISIBLE else GONE
    }

    fun setQuickProductsViewVisible(visible: Boolean) {
        binding.quickProductsView.visibility = if (visible) VISIBLE else GONE
    }

}