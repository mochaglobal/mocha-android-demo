package com.mocha.demokeyboard

import android.view.inputmethod.EditorInfo
import com.mocha.sdk.KeyboardContext
import com.mocha.sdk.KeyboardContextRule

class CustomBrowserKeyboardContextRule : KeyboardContextRule {

    override fun process(editorInfo: EditorInfo): KeyboardContext {
        return if (editorInfo.packageName in BROWSER_PACKAGE_NAMES) KeyboardContext.BROWSER else KeyboardContext.NONE
    }

    private companion object {

        private val BROWSER_PACKAGE_NAMES = listOf(
            "com.apusapps.browser"
        )
    }

}